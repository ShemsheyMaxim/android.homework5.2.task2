package com.maxim.commodityturnoverrecyclerview;

import android.content.res.Resources;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import android.widget.Toast;

import com.maxim.commodityturnoverrecyclerview.model.Product;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "test"; // here can be any String
    private AdapterProduct adapterProduct;
    private List<Product> products = new ArrayList<Product>();

    final int STATUS_NOT_CHANGE = 0;
    final int STATUS_FIRST_OPEN = 1;
    final int STATUS_CLOSE = 2;
    final int STATUS_OPEN = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getData();
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        adapterProduct = new AdapterProduct(this, products);
        recyclerView.setAdapter(adapterProduct);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayout.HORIZONTAL);
        recyclerView.setLayoutManager(llm);

        final Button firstOpen = (Button) findViewById(R.id.button_first_open);
        final Button open = (Button) findViewById(R.id.button_open);
        final Button close = (Button) findViewById(R.id.button_close);

        final Handler handler = new Handler() {
            Queue<Integer> queueOfBuyers = new LinkedList<>();
            int currentStatusShop = -1;

            @Override
            public void handleMessage(Message msg) {

                if (msg.what != STATUS_NOT_CHANGE) {
                    currentStatusShop = msg.what;
                }

                switch (currentStatusShop) {
                    case STATUS_FIRST_OPEN:
                        for (int i = 0; i <= msg.arg1; i++) {
                            final Product enterProduct = products.get(i);
                            final int newCount = enterProduct.getCountProducts() - 1;
                            enterProduct.setCountProducts(newCount);
                        }
                        for (int i = 0; i <= products.size() - 1; i++) {
                            if (products.get(i).getCountProducts() <= 0) {
                                products.remove(i);
                            }
                        }
                        break;
                    case STATUS_CLOSE:
                        queueOfBuyers.offer(msg.arg1);
                        Log.i(TAG, "offer " + msg.arg1);
                        break;
                    case STATUS_OPEN:
                        while (!queueOfBuyers.isEmpty()) {
                            Integer countBoughtProductBuyerFromTheQueue = queueOfBuyers.poll();
                            Log.i(TAG, "queueOfBuyers.poll " + countBoughtProductBuyerFromTheQueue);
                            if (countBoughtProductBuyerFromTheQueue != null) {
                                for (int i = 0; i < countBoughtProductBuyerFromTheQueue; i++) {
                                    final Product enterProduct = products.get(i);
                                    final int newCount = enterProduct.getCountProducts() - 1;
                                    enterProduct.setCountProducts(newCount);
                                }
                                for (int i = 0; i <= products.size() - 1; i++) {
                                    if (products.get(i).getCountProducts() <= 0) {
                                        products.remove(i);
                                    }
                                }
                            }
                        }
                        if (queueOfBuyers.isEmpty()) {
                            for (int i = 0; i <= msg.arg1; i++) {
                                final Product enterProduct = products.get(i);
                                final int newCount = enterProduct.getCountProducts() - 1;
                                enterProduct.setCountProducts(newCount);
                            }
                            for (int i = 0; i <= products.size() - 1; i++) {
                                if (products.get(i).getCountProducts() <= 0) {
                                    products.remove(i);
                                }
                            }
                        }
                        break;
                }
                adapterProduct.notifyDataSetChanged();


                if (products.size() == 0) {
                    open.setEnabled(false);
                    close.setEnabled(false);
                    Toast toast = Toast.makeText(getApplicationContext(), "All products were sell", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }
        };

        firstOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setVisibility(View.GONE);
                open.setVisibility(View.VISIBLE);
                open.setEnabled(false);
                close.setEnabled(true);

                Message msg1 = handler.obtainMessage(STATUS_FIRST_OPEN);
                handler.sendMessage(msg1);
                Log.i(TAG, "STATUS = " + msg1.what);

                final Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (products.size() != 0) {
                            Random random = new Random();
                            int countBoughtProduct = random.nextInt(products.size());

                            Message msg = handler.obtainMessage(STATUS_NOT_CHANGE, countBoughtProduct, 0);
                            handler.sendMessage(msg);
                            Log.i(TAG, "STATUS = " + msg.what);
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }

                });
                thread.start();
            }
        });

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                open.setEnabled(true);

                Message msg2 = handler.obtainMessage(STATUS_CLOSE, 0, 0);
                handler.sendMessage(msg2);
                Log.i(TAG, "STATUS = " + msg2.what);
            }
        });

        open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.setEnabled(false);
                close.setEnabled(true);

                Message msg3 = handler.obtainMessage(STATUS_OPEN, 0, 0);
                handler.sendMessage(msg3);
                Log.i(TAG, "STATUS = " + msg3.what);
            }
        });
    }

    private void getData() {

        Resources resources = getResources();
        String[] namesProduct = resources.getStringArray(R.array.name_product);
        int[] countProduct = resources.getIntArray(R.array.count_product);

        for (int i = 0; i < namesProduct.length; i++) {
            products.add(new Product(namesProduct[i], countProduct[i]));
        }
    }
}
