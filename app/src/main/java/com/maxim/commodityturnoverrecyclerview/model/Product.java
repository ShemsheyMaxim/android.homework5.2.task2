package com.maxim.commodityturnoverrecyclerview.model;

import java.io.Serializable;

public class Product implements Serializable {

    private String nameProduct;
    private int countProducts;

    public Product(String nameProduct, int countProducts) {
        this.nameProduct = nameProduct;
        this.countProducts = countProducts;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public int getCountProducts() {
        return countProducts;
    }

    public void setCountProducts(final int newCountProduct) {
        countProducts = newCountProduct;
    }
}