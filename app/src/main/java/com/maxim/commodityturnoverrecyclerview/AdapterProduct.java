package com.maxim.commodityturnoverrecyclerview;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.maxim.commodityturnoverrecyclerview.model.Product;


import java.util.List;

/**
 * Created by Максим on 20.05.2017.
 */

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ViewHolder> {

    private List<Product> products;
    private Context context;

    public AdapterProduct(Context context, List<Product> products) {
        this.context = context;
        this.products = products;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(context).inflate(R.layout.item_view, parent, false);
        return new ViewHolder(item);
    }

    @Override
    public void onBindViewHolder(AdapterProduct.ViewHolder holder, int position) {
        Product product = products.get(position);
        holder.nameProduct.setText(product.getNameProduct());
        holder.countProduct.setText(String.valueOf(product.getCountProducts()));

    }

    @Override
    public int getItemCount() {
        return products.size();
    }

     class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nameProduct;
        private TextView countProduct;

        public ViewHolder(View item) {
            super(item);
            nameProduct = (TextView) item.findViewById(R.id.textView_name_product);
            countProduct = (TextView) item.findViewById(R.id.textView_count_product);

        }
    }
}
